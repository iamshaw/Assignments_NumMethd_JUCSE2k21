#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double f(double x) {
	// return the functional value
	// return (exp(-x) - x);
	return (exp(x) - 4 * pow(x, 2));
}
	
int main() {
	FILE *fp = fopen("false_position_result.txt", "w");	// file to save the result in tabular form
	double a = 0,										// interval limit-1
		   b = 0,										// interval limit-2
		   c = 0,										// stores point where line from f(a) and f(b) intersects the x-axis
		   rel_error = 0,								// stores value of relative error
   		   prev_error = 0,
   		   abs_error = 0,
		   convergence = 0,
		   prev_c = 0;									// saves previous value of c for calculating the relative error
	char sign = '-';									// store sign of f(a)*f(b)
	int iter = 0;										// iteration
	FILE *data = fopen("false_position_root.csv", "w");
	
	printf("Enter a, b:\n");
	scanf("%lf %lf", &a, &b);							// take input in a & b
	while(f(a)*f(b) > 0) {								// make sure input a & b is such that f(a)*f(b) < 0
		printf("Enter proper values for a, b. f(a)*f(b) must be negative...\n");
		scanf("%lf %lf", &a, &b);
	}
	
	fprintf(fp, "Given inputs a, b: %lf, %lf\n", a, b);
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	fprintf(fp, "|  iter  |          a  |           b  |           c  |        f(a)  |        f(c)  |   f(a)*f(c)  |     sign  |   convergence   |\n");
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	
	c = (a * f(b) - b * f(a)) / (f(b) - f(a));
	prev_c = c;

	while(f(c) > 0.0000005 || f(c) < -0.0000005) {
	++iter;
	sign = (f(a) * f(c) < 0) ? '-': '+';
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.6lf    |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), sign), convergence;
		if(f(a) * f(c) < 0)
			b = c;									// change interval limit-2
		else
			a = c;									// change interval limit-1
		prev_c = c;
		c = (a * f(b) - b * f(a)) / (f(b) - f(a));
		prev_error = rel_error;
		rel_error = (c - prev_c) / c;				// calculate relative error
		abs_error = fabs(c - prev_c);
		convergence = fabs(log(rel_error)/log(prev_error));
		fprintf(data, "%d,%lf,%lf\n", iter, c, abs_error);
	}

	fprintf(fp, "------------------------------------------------------------------------------------------------------------------------------------\n");	
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.6lf    |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), sign, convergence);
	fprintf(fp, "------------------------------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", c);
	printf("Relative error is: %lf\n", rel_error);

	fprintf(fp, "Root in the given interval: %lf\n", c);
	fprintf(fp, "Relative error is: %lf\n", rel_error);
	fclose(fp);
	
	return 0;
}
