#include <stdio.h>

#define MAX 10

double A[MAX][2 * MAX]; // VAR[MAX];

void print_arr(double matrix[MAX][2 * MAX], int x, int y) {
    int i, j;
    printf("\n\n    Matrix:\n");
    for(i = 0; i < x; ++i) {
        for(j = 0; j < y; ++j)
            printf("  %lf  ", matrix[i][j]);
        printf("\n");
    }
}

void swap(double *a, double *b) {
    double temp = *a;
    *a = *b;
    *b = temp;
}

// void init_soln(int n) {
//     while(n) {
//         VAR[n] = 0;
//         --n;
//     }
// }

int main() {
    int n;
    printf("Enter number of rows of square matrix: ");
    scanf("%d", &n);
    int i, j;
    for(i = 0; i < n; ++i) {
        printf("\n Enter row %d:\n", i+1);
        for(j = 0; j < n; ++j) {
            printf("\n  Enter column %d of row %d: ", j + 1, i + 1);
            scanf("%lf", &A[i][j]);
        }
        for(j = n; j < 2 * n; ++j) {
            if(j == n + i) {
                printf("A[%d][%d] = 1\n", i + 1, j + 1);
                A[i][j] = 1;
            }
            else {
                printf("A[%d][%d] = 1\n", i + 1, j + 1);
                A[i][j] = 0;
            }
        }
    }
    print_arr(A, n, 2 * n);

    double temp_max = 0;
    int max_row;
    for(j = 0; j < n; ++j) {
        temp_max = A[j][j];
        max_row = j;
        for(i = j; i < n; ++i) {
            if(A[i][j] > temp_max) {
                temp_max = A[i][j];
                max_row = i;
            }
        }
        if(max_row != j) {
            int x = j;
            for(; x < 2 * n; ++x) {
                swap(&A[j][x], &A[max_row][x]);
            }
        }
        printf("\n  Swapped row %d with row %d:\n", j + 1, max_row + 1);
        print_arr(A, n, 2 * n);

        int x, y;
        for(x = 0; x < n; ++x) {
            if(x == j) {
                double multip = 1 / A[j][j];
                for(y = 0; y < 2 * n; ++y)
                    A[x][y] *= multip;
                printf("\n  Augmented matrix:\n");
                printf("  Multiplier for row %d is %lf.\n", x + 1, multip);
                print_arr(A, n, 2 * n);  
            }
            else {
                double multip = A[x][j] / A[j][j];
                for(y = 0; y < 2 * n; ++y)
                    A[x][y] -= A[j][y] * multip;
                printf("\n  Augmented matrix:\n");
                printf("  Multiplier for row %d is %lf.\n", x + 1, multip);
                print_arr(A, n, 2 * n);
            }
        }
    }
    // for(j = 0; j < n; ++j) {

    // }

    printf("\nSeparating inverse now...\n\n");
    for(i = 0; i < n; ++i) {
        for(j = n; j < 2 * n; ++j)
            printf("  %lf  ", A[i][j]);
        printf("\n");
    }

    return 0;
}