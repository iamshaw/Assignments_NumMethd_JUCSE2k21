#include <stdio.h>
#include <math.h>

#define MAX 10
#define ACCURACY 0.000005

double EQN[MAX][MAX+1], X[MAX];

void print_arr(double mat[MAX][MAX+1], int n) {
	int i, j;
	printf("\n\n    Matrix...\n");
	for(i = 0; i < n; ++i) {
		for(j = 0; j < n + 1; ++j) {
			printf("  %lf  ", mat[i][j]);
		}
		printf("\n");
	}
	printf("\n\n");
}

void print_soln(int n) {
	int i;
	printf("\n\n    Solution Matrix: \n");
	for(i = 0; i < n; ++i)
		printf("  %lf  ", X[i]);
	printf("\n\n");
}

int main() {
	int n;
	printf("Enter number of variables: ");
	scanf("%d", &n);
	while(!n) {
		printf("\nEnter proper value of n:");
		scanf("%d", &n);
	}
	int i, j;
	for(i = 0; i < n; ++i) {
		printf("\n Enter EQN %d...\n", i + 1);
		for(j = 0; j < n; ++j) {
			printf("\n  Enter coefficient of x_%d for eqn %d: ", j + 1, i + 1);
			scanf("%lf", &EQN[i][j]);
			if(i == j) {
				if(EQN[i][i] == 0) {
					printf("Diagonal elements can not be zero.\n");
					printf("Aborting...\n");
					return 0;
				}
			}
		}
		printf("\n  Enter constant value of this equation: ");
		scanf("%lf", &EQN[i][n]);
	}
	print_arr(EQN, n);

	printf("Initializing value of all variables with 0.\n");
	int z;
	// for(z = 0; z < n; ++z)
	// 	X[z] = 1.0;
	X[0] = 0.186;
	X[1] = 0.331;
	X[2] = -0.423;
	print_soln(n);

	double dif_max = ACCURACY * 2;
	int iter = 0, equal_ctr = 0;
	printf("iter");
	for(z = 0; z < n; ++z)
		printf("        X[%d]", z + 1);
	printf("    dif_max\n");
	while(fabs(dif_max) > ACCURACY) {
		++iter;
		equal_ctr = 0;
		dif_max = 0;
		for(i = 0; i < n; ++i) {
			double divider = 1, subtr = 0.0;
			for(j = 0; j < n; ++j) {
				if(i == j)
					divider = EQN[i][i];
				else {
					subtr += X[j] * EQN[i][j];
				}
			}
			double temp_val = (EQN[i][n] - subtr) / divider;
			if(fabs(temp_val - X[i]) > dif_max)
				dif_max = temp_val - X[i];
			X[i] = temp_val;
			if(fabs(dif_max - 2*ACCURACY) < ACCURACY)
				break;
		}
		int k;
		printf("%4d", iter);
		for(k = 0; k < n; ++k) {
			printf("  %10.7lf", X[k]);
		}
		printf("  %10.8lf\n", dif_max);
		if(iter > 500) {
			printf("Way too many iterations... Aborting..\n");
			return 0;
		}
	}

	printf("\n\n");
	print_soln(n);
	return 0;
}