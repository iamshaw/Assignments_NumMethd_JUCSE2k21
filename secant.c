#include <stdio.h>
#include <math.h>

#define g_x sqrt(exp(x) / 4)
#define dg_x (exp(x/2) / 4)
#define f1 (exp(x) - 4 * pow(x, 2))

double f(double x) {
	return f1;
}

double dg(double x) {
	return dg_x;
}

double next_x(double x, double prev_x) {
	return (prev_x * f(x) - x * f(prev_x)) / (f(x) - f(prev_x));
}


double order_conv(double error, double prev_error) {
	return (log(error) / log(prev_error));
}

void secant(double x, double x_prev) {
	FILE *fp = fopen("secant_root.csv", "w");
	double x_new, error=0, prev_error=x, root = 0, conv;
	int root_found = 0;
	int iter = 0;

	printf(" i|        x|    dg(x)|     f(x)|    abs_err| ord_conv\n");
	printf("------------------------------------------------------\n");
	do {
		++iter;
		x_new = next_x(x, x_prev);
		error = fabs(x_new - x);
		if(f(x_new) < 0.0000005 && f(x_new) > -0.0000005) {
			root = x_new;
			++root_found;
		}
		if(fabs(x_new - x) < 0.0000005) {
			root = x_new;
			++root_found;
		}
		if(iter > 1) {
			conv = fabs(log(error) / log(prev_error));
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf| %12.10lf\n", iter, x_new, dg(x_new), f(x_new), error, conv);
		}
		else
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf|     -    \n", iter, x_new, dg(x_new), f(x_new), error, conv);
		prev_error = error;
		x_prev = x;
		x = x_new;
		fprintf(fp, "%d,%lf,%lf\n", iter, x_new, error);
	} while(iter <= 50 && !root_found);
	if (root_found)
		printf("Root: %lf\n", root);
	else
		printf("No root found.\n");
	fclose(fp);
}

int main() {
	double x = 0.0, x2 = 0.0;
	printf("Enter your initial guesses(x_1, x_2): ");
	scanf("%lf""%lf", &x, &x2);
	secant(x, x2);
	return 0;
}