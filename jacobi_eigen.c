#include <stdio.h>
#include <math.h>

#define MAX 10

double A[MAX][MAX], temp_A[MAX][MAX];

int signum(int num) {
    if(num > 0)
        return 1;
    if(num == 0)
        return 0;
    return -1;
}

void copy_mat(double src[MAX][MAX], double dest[MAX][MAX], int size) {
    int i, j;
    for(i = 0; i < size; ++i)
        for(j = 0; j < size; ++j)
            dest[i][j] = src[i][j];
}

void print_mat(int size) {
    int i, j;
    printf("\nMatrix A:\n\n");
    for(i = 0; i < size; ++i) {
        for(j = 0; j < size; ++j)
            printf("  %10.6lf", A[i][j]);
        printf("\n");
    }
    printf("\n\n");
}

void jacobi_eigen() {
    int size;
    printf("Enter size of matrix: ");
    scanf("%d", &size);
    printf("Enter the lower triangular + diagonal elements of the symmetric matrix:\n");
    int i, j, k;
    for(i = 0; i < size; ++i) {
        for(j = 0; j <= i; ++j) {
            printf("Enter element A[%d][%d]: ", i, j);
            scanf("%lf", &A[i][j]);
            A[j][i] = A[i][j];
            print_mat(size);
        }
    }
    printf("\n\n======Calculation======\n\n");
    for(k = 0; k < size*(size-1)/2; ++k) {
        print_mat(size);
        int p, q;
        double mx = 0.0;
        for(i = 0; i < size; ++i) {
            for(j = 0; j < size; ++j) {
                if(i != j && fabs(A[j][i]) > mx) {
                    q = i;
                    p = j;
                    mx = fabs(A[j][i]);
                    printf("\nCurrent mx=%lf for q=%d, p=%d\n", mx, q, p);
                }
            }
        }
        printf("\n  Found max at pos (%d, %d). Max is %lf.\n", q, p, A[q][p]);
        double alpha, beta, cos_theta, sin_theta;
        alpha = 2*signum(A[q][q] - A[p][p])*A[q][p];
        beta = fabs(A[q][q] - A[p][p]);
        printf("\n  alpha=%lf\tbeta=%lf", alpha, beta);
        cos_theta = sqrt(0.5 * (1 + (beta / sqrt(alpha*alpha + beta*beta))));
        sin_theta = (1/(2 * cos_theta))*(alpha / sqrt(alpha*alpha + beta*beta));
        printf("\n  cos_theta=%lf\tsin_theta=%lf\n\n", cos_theta, sin_theta);
        copy_mat(A, temp_A, size);
        for(i = 0; i < size; ++i) {
            if(i != q && i != p) {
                temp_A[q][i] = A[q][i]*cos_theta + A[p][i]*sin_theta;
                temp_A[p][i] = -A[q][i]*sin_theta + A[p][i]*cos_theta;
                temp_A[i][q] = A[i][q]*cos_theta + A[i][p]*sin_theta;
                temp_A[i][p] = -A[i][q]*sin_theta + A[i][p]*cos_theta;
            }
            double temp_qq, temp_pp;
            temp_qq = A[q][q]*cos_theta*cos_theta + 2*A[q][p]*sin_theta*cos_theta + A[p][p]*sin_theta*sin_theta;
            temp_pp = A[q][q]*sin_theta*sin_theta - 2*A[q][p]*sin_theta*cos_theta + A[p][p]*cos_theta*cos_theta;
            temp_A[q][q] = temp_qq;
            temp_A[p][p] = temp_pp;
            temp_A[q][p] = temp_A[p][q] = 0;
        }
        copy_mat(temp_A, A, size);
        printf("Modified matrix: ");
        print_mat(size);
        printf("-----------------\n");
    }
    printf("\n\nObtained eigen values are:\n");
    for(i = 0; i < size; ++i)
        printf("%10.6lf\n", A[i][i]);

}

int main(void) {
    jacobi_eigen();
    return 0;
}


/*
Input:

4 7 3 9 2 -2 -4 1 4 2 3

*/