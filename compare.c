#include <stdio.h>
#include <math.h>

#define f1 (pow(x, 2) - 2)
#define df1 (2 * x)
#define ddf1 (2)

#define f2 (pow(x, 3) - 2 * x - 5)
#define df2 (3 * pow(x, 2) - 2)
#define ddf2 (6 * x)

#define f3 (x - 2 + log(x))
#define df3 (1 + (1 / x))
#define ddf3 (-1 / pow(x, 2))

#define f4 (pow(x, 3) - 3 * x - 5)
#define df4 (3 * pow(x, 2) - 3)
#define ddf4 (6 * x)

#define f5 ((x * sin(x)) + cos(x))
#define df5 (x * cos(x))
#define ddf5 (cos(x) - (x * sin(x)))

#define f6 (exp(x) - 2 * x - 1)
#define df6 (exp(x) - 2)
#define ddf6 (exp(x))

double f(double x) {
	return f6;
}

double df(double x) {
	return df6;
}

double ddf(double x) {
	return ddf6;
}

int newton_raphson(double x1) {
	double x2, x_temp, error = 1, error_prev, ord_conv;
	int iter = 0;
	printf("   i         x_i      f(x_i)   abs_error    ord_conv\n");
	printf("----------------------------------------------------\n");
	do {
		++iter;
		error_prev = error;
		x_temp = x1;
		x2 = x1 - (f(x1) / df(x1));
		x1 = x2;
		error = x2 - x_temp;
		ord_conv = (log(fabs(error)) / log(fabs(error_prev)));
		if(iter == 1)
			printf("%4d  %10.6lf  %10.6lf  %10.6lf  ----na----\n", iter, x1, f(x1), error, ord_conv);
		else
			printf("%4d  %10.6lf  %10.6lf  %10.6lf  %10.6lf\n", iter, x1, f(x1), error, ord_conv);
	} while(fabs(x2 - x_temp) > 0.00001);
	printf("----------------------------------------------------\n");
	printf("\nRoot is: %lf\n", x2);
	return iter;
}

int bisection(double a, double b) {
	double mid = 0,									// mid-value of a & b
		   rel_error = 0,							// stores value of relative error
		   prev_error = 0,
		   convergence = 0,
		   prev_mid = 0;							// saves previous value of mid for calculating the relative error
	char sign = '-';								// store sign of f(a)*f(b)
	int iter = 0;									// iteration
	
	printf("Given inputs a, b: %lf, %lf\n", a, b);
	printf("---------------------------------------------------------------------------------------------------------------------------------\n");
	printf("|  iter  |          a  |           b  |           c  |        f(a)  |        f(c)  |   f(a)*f(c)  |     sign  |   convergence   |\n");
	printf("---------------------------------------------------------------------------------------------------------------------------------\n");
	
	mid = (a + b) / 2;
	prev_mid = mid;

	while(f(mid) > 0.0000005 || f(mid) < -0.0000005) {
	++iter;
	sign = (f(a) * f(mid) < 0) ? '-': '+';
	printf("|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.9lf    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), sign), convergence;
		if(f(a) * f(mid) < 0)
			b = mid;								// change interval limit-2
		else
			a = mid;								// change interval limit-1
		prev_mid = mid;
		mid = (a + b) / 2;
		prev_error = rel_error;
		rel_error = (mid - prev_mid) / mid;			// calculate relative error
		convergence = fabs(log(fabs(rel_error))/log(fabs(prev_error)));

	}

	printf("------------------------------------------------------------------------------------------------------------------------------------\n");	
	printf("|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.9lf    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), sign, convergence);
	printf("------------------------------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", mid);
	printf("Relative error is: %lf\n", rel_error);
	
	return iter;
}


int regular_falsi(double a, double b) {
	double c = 0,										// stores point where line from f(a) and f(b) intersects the x-axis
		   rel_error = 0,								// stores value of relative error
   		   prev_error = 0,
		   convergence = 0,
		   prev_c = 0;									// saves previous value of c for calculating the relative error
	char sign = '-';									// store sign of f(a)*f(b)
	int iter = 0;										// iteration
	
	printf("Given inputs a, b: %lf, %lf\n", a, b);
	printf("---------------------------------------------------------------------------------------------------------------------------------\n");
	printf("|  iter  |          a  |           b  |           c  |        f(a)  |        f(c)  |   f(a)*f(c)  |     sign  |   convergence   |\n");
	printf("---------------------------------------------------------------------------------------------------------------------------------\n");
	
	c = (a * f(b) - b * f(a)) / (f(b) - f(a));
	prev_c = c;

	while(f(c) > 0.0000005 || f(c) < -0.0000005) {
	++iter;
	sign = (f(a) * f(c) < 0) ? '-': '+';
	printf("|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.9lf    |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), sign), convergence;
		if(f(a) * f(c) < 0)
			b = c;									// change interval limit-2
		else
			a = c;									// change interval limit-1
		prev_c = c;
		c = (a * f(b) - b * f(a)) / (f(b) - f(a));
		prev_error = rel_error;
		rel_error = (c - prev_c) / c;				// calculate relative error
		convergence = fabs(log(fabs(rel_error)/log(fabs(prev_error))));
	}

	printf("------------------------------------------------------------------------------------------------------------------------------------\n");	
	printf("|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %10.9lf    |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), sign, convergence);
	printf("------------------------------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", c);
	printf("Relative error is: %lf\n", rel_error);
	
	return iter;
}

int main() {
	double a, b;
	do {
		printf("Enter a, b such that f(a) * f(b) is negative...\n");
		scanf("%lf""%lf", &a, &b);
	}
	while(f(a) * f(b) > 0);
	int iter_bisec, iter_regfalsi, iter_newton;
	printf("\n\nGoing for Bisection method\n:");
	iter_bisec = bisection(a, b);
	printf("\n\nGoing for Regular Falsi method\n:");
	iter_regfalsi = regular_falsi(a, b);
	printf("Passing a=%lf as argument for Newton Raphson:\n");
	iter_newton = newton_raphson(a);
	printf("\n\nNewton Raphson took %d iterations, Bisection took %d iterations and Regular Falsi took %d iterations\n", iter_newton, iter_bisec, iter_regfalsi);
	return 0;
}