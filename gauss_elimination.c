#include <stdio.h>
#include <math.h>

#define MAX 10
#define ACCURACY 0.0000005

double A[MAX][MAX+1], M[MAX][MAX+1], X[MAX];

void printMatrix(int row, int col, double m[MAX][MAX+1]) {
	int i, j;
	printf("\n");
	printf("Matrix now is:\n");
	for(i = 0; i < row; ++i) {
		for(j = 0; j < col; ++j)
			printf(" %lf ", m[i][j]);
		printf("\n");
	}
	printf("\n");
}

int main() {
	printf("Enter number of variables: ");
	int n;
	scanf("%d", &n);
	printf("Enter coefficients:\n");
	int i, j;
	for(i = 0; i < n; ++i) {
		printf("  Enter coefficents of equation %d: \n", i+1);
		for(j = 0; j < n; ++j) {
			printf("    Enter coefficent of variable %d:", j+1);
			scanf("%lf", &A[i][j]);
		}
		printf("    Enter value of this equation: ");
		scanf("%lf", &A[i][j]);
	}

	printMatrix(n, n+1, A);

	int k, p, x;
	for(k = 0; k < n; ++k) {
		double max = ACCURACY;
		for(x = k+1; x < n; ++x) {
			if(fabs(A[x][k]) > max) {
				p = x;
				max = fabs(A[x][k]);
			}
			printf("Max is %lf\n", max);
			printf("A[%d][%d] > %lf\n", x, k, max);
		}
		if(p != k) {
			int y;
			for(y = 0; y < n+1; ++y) {
				printf("Swapping %d & %d\n", p, k);
				double temp = 0.0;
				temp = A[p][y];
				A[p][y] = A[k][y];
				A[k][y] = temp;
			}
		}
		printMatrix(n, n+1, A);
		printf("Converting to Echelon form...");
		for(i = k + 1; i <= n; ++i) {
			M[i][k] = A[i][k] / A[k][k];
			for(j = k; j < n+2; ++j)
				A[i][j] = A[i][j] - A[k][j] * M[i][k];
		}
		printMatrix(n, n+1, A);
	}
	X[n - 1] = A[n - 1][n] / A[n - 1][n - 1];
	for(i = n - 2; i > 0; +--i) {
		double S;
		for(j = i + 1; j < n; ++j) {
			S = A[i][n];
			S = S - A[i][j] * X[j];
		}
		X[i] = S / A[i][i];
	}

	printf("Roots are:\n");
	for(i = 0; i < n; ++i)
		printf("  X[%d] = %lf\n", i+1, X[i]);
	return 0;
}