#include <stdio.h>
#include <math.h>

#define MAX 10
#define PRECISION 0.0000005

double A[MAX][MAX], V[MAX], X[MAX], X_prev[MAX];

void copy_X(double *src, double *target, int n) {
    // function for copying contents of X to X_prev
    int i;
    for(i = 0; i < n; ++i)
        *(target + i) = *(src + i);
}

void init(int n) {
    // function to initialise X and X prev with 0's and 1's
    // X_prev stores the initial assumptions of the Eigen vector
    int i;
    for(i = 0; i < n; ++i) {
        X[i] = 0;
        X_prev[i] = 1;
    }
}

double calc_max(double *new, double *prev, int n) {
    // function to calculate max difference between X and X_prev
    if(n > 0) {
        int i;
        double max = fabs(new[0] - prev[0]);
        for(i = 0; i < n; ++i) {
            if(fabs(new[i] - prev[i]) > max)
                max = fabs(new[i] - prev[i]);
        }
        return max;
    }
    else
        return 0;
}

void print_A(int side) {
    // function to print A
    int i, j;
    printf("\n");
    for(i = 0; i < side; ++i) {
        for(j = 0; j < side; ++j)
            printf("  %lf  ", A[i][j]);
        printf("\n");
    }
    printf("\n");
}

void print_X(double *mat, int n) {
    // function to print any one of X or X_prev
    int i;
    printf("\n");
    for(i = 0; i < n; ++i)
        printf("  %lf\n", mat[i]);
    printf("\n");
}

int main() {
    int side;
    printf("Number of elements in each side of square matrix: ");
    scanf("%d", &side);
    int i, j;
    for(i = 0; i < side; ++i)
        for(j = 0; j < side; ++j) {
            printf("  Enter element (%d, %d): ", i + 1, j + 1);
            scanf("%lf", &A[i][j]);
        }
    init(side);

    printf("\n\n  Matrix A:\n");
    print_A(side);
    printf("\n  Matrix X:\n");
    print_X(X, side);
    printf("\n  Matrix X_prev:\n");
    print_X(X_prev, side);

    int iter = 0;
    double error = 0;
    double C;
    do {
        ++iter;
        printf("\nIteration: %d\n", iter);
        int i, j, k;
        for(i = 0; i < side; ++i) {
            X[i] = 0;
            for(j = 0; j < side; ++j)
                X[i] += A[i][j] * X_prev[j];        // Matrix multiplication of X = A * X_prev
        }
        printf("\n  Matrix X:\n");
        print_X(X, side);
        double max = X[0];
        for(i = 0; i < side; ++i)
            if(X[i] > max)
                max = X[i];                         // Calculating max element obtained in X
        C = max;
        printf("\n  C = %lf\n", C);
        for(j = 0; j < side; ++j)
            X[j] = X[j] / C;                        // Normalizing vector X
        printf("\n  Normalized X:\n");
        print_X(X, side);
        error = calc_max(X, X_prev, side);          // Calculating max differnece between X and X_prev
        copy_X(X, X_prev, side);                    // Copying X to X_prev
        printf("\n  Matrix X_prev:\n");
        print_X(X_prev, side);
        printf("    Accuracy = %12.8lf\n", error);
        
    } while(error > PRECISION && iter < 50);

    printf("  Largest Eigen Value: %12.8lf\n", C);
    printf("  Eigen Vector:\n");
    print_X(X, side);
    return 0;
}