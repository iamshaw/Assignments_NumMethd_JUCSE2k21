#include <stdio.h>
#include <math.h>

#define MAX 10
#define ACCURACY 0.0000005

float A[MAX], B[MAX], C[MAX], D[MAX];

void solve_quad(double A, double B, double C) {
    double disc = B * B - 4 * A * C;
    int complex = 0;
    if(disc < 0) {
        disc = -disc;
        complex = 1;
    }

    double root1_real, root1_img, root2_real, root2_img;
    root1_real = -B / (2 * A);
    root2_real = root1_real;
    root1_img = sqrt(disc) / (2 * A);
    root2_img = -root1_img;
    if(complex) {
        printf("Root 1 is: (%lf) + i(%lf)\n", root1_real, root1_img);
        printf("Root 2 is: (%lf) + i(%lf)\n", root2_real, root2_img);
    }
    else {
        printf("Root 1 is: %lf\n", root1_real + root1_img);
        printf("Root 2 is: %lf\n", root2_real + root2_img);
    }
}

int main() {
    float p, q, r, s, error, del_p, del_q, dr_dq, ds_dq, dr_dp, ds_dp;
    int i, degree, iter = 0;

    printf("Enter degree of polynomial: ");
    scanf("%d", &degree);

    for(i = degree; i >= 0; --i) {
        printf("Enter coefficient of x^%d: ", i);
        if(i < degree) {
            scanf("%f", &A[degree-1-i]);
            A[degree-1-i] /= A[degree];     // Dividing each coefficient by coefficient of highest degree
        }
        else
            scanf("%f", &A[i]);
    }
    A[degree] = 1;                          // Making sure x^(highest power) is 1

    printf("Enter approx. values of P and Q respectively in X^2 + PX + Q: ");
    scanf("%f""%f", &p, &q);

    error = 1;
    while(error >= ACCURACY) {
        // Lin's method
        for(i = 0; i <= degree - 3; ++i)
            if(i == 0)
                B[i] = A[i] - p;
            else if(i == 1)
                B[i] = A[i] - p * B[i - 1] - q;
            else
                B[i] = A[i] - p * B[i - 1] - q * B [i - 2];
        
        // Bairstow's Method
        r = A[degree - 2] - p * B[degree - 3] - q * B[degree - 4];
        s = A[degree - 1] - q * B[degree - 3];

        for(i = 0; i <= degree - 3; ++i)
            if(i == 0)
                C[i] = -1;
            else if(i == 1)
                C[i] = -B[0] + p;
            else
                C[i] = B[i - 1] - p * C[i - 1] - q * C[i - 2];
        
        dr_dp = -B[degree - 3] - p * C[degree - 3] - q * C[degree - 4];
        ds_dp = -q * C[degree - 3];

        for(i = 0; i <= degree - 3; ++i)
            if(i == 0)
                D[i] = 0;
            else if(i == 1)
                D[i] = -1;
            else
                D[i] = -B[i - 2] - p * D[i - 1] - q * D[i - 2];
        
        dr_dq = -B[degree - 4] - p * D[degree - 3] - q * D[degree - 4];
        ds_dq = -B[degree - 3] - q * D[degree - 3];

        del_p = (s * dr_dq - r * ds_dq) / (dr_dp * ds_dq - ds_dp * dr_dq);
        p += del_p;
        del_q = (r * ds_dp - s * dr_dp) / (dr_dp * ds_dq - ds_dp * dr_dq);
        q += del_q;

        del_p = fabs(del_p);
        del_q = fabs(del_q);

        if(del_p > del_q)
            error = del_p;
        else
            error = del_q;
        
        ++iter;
    }

    printf("X^2 + %f*X + %f\n", p, q);
    printf("Iterations: %d\n", iter);
    solve_quad(1, p, q);

    return 0;
}