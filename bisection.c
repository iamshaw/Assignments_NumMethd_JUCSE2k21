#include <stdio.h>
#include <math.h>
#include <stdlib.h>

double f(double x) {
	// return the functional value
	// return (exp(-x) - x);
	return (exp(x) - 4 * pow(x, 2));
}
	
int main() {
	FILE *fp = fopen("bisection_result.txt", "w");	// file to save the result in tabular form
	double a = 0,									// interval limit-1
		   b = 0,									// interval limit-2
		   mid = 0,									// mid-value of a & b
		   rel_error = 0,							// stores value of relative error
		   prev_error = 0,
		   abs_error = 0,
		   convergence = 0,
		   prev_mid = 0;							// saves previous value of mid for calculating the relative error
	char sign = '-';								// store sign of f(a)*f(b)
	int iter = 0;									// iteration
	FILE *data = fopen("bisection_root.csv", "w");

	printf("Enter a, b:\n");
	scanf("%lf %lf", &a, &b);						// take input in a & b
	while(f(a)*f(b) > 0) {							// make sure input a & b is such that f(a)*f(b) < 0
		printf("Enter proper values for a, b. f(a)*f(b) must be negative...\n");
		scanf("%lf %lf", &a, &b);
	}
	
	fprintf(fp, "Given inputs a, b: %lf, %lf\n", a, b);
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------\n");
	fprintf(fp, "|  iter  |          a  |           b  |         mid  |        f(a)  |      f(mid)  | f(a)*f(mid)  |     sign  |\n");
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------\n");
	
	mid = (a + b) / 2;
	prev_mid = mid;

	while(f(mid) > 0.0000005 || f(mid) < -0.0000005) {
	++iter;
	sign = (f(a) * f(mid) < 0) ? '-': '+';
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |    %lf    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), sign), convergence;
		if(f(a) * f(mid) < 0)
			b = mid;								// change interval limit-2
		else
			a = mid;								// change interval limit-1
		prev_mid = mid;
		mid = (a + b) / 2;
		prev_error = rel_error;
		rel_error = (mid - prev_mid) / mid;			// calculate relative error
		abs_error = fabs(mid - prev_mid);
		convergence = fabs(log(rel_error)/log(prev_error));
		fprintf(data, "%d,%lf,%lf\n", iter, mid, abs_error);

	}

	fprintf(fp, "-------------------------------------------------------------------------------------------------------------\n");	
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |      %c    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), sign);
	fprintf(fp, "-------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", mid);
	printf("Relative error is: %lf\n", rel_error);

	fprintf(fp, "Root in the given interval: %lf\n", mid);
	fprintf(fp, "Relative error is: %lf\n", rel_error);
	fclose(fp);
	fclose(data);
	return 0;
}
