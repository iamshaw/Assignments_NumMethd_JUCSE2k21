#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double f(double x) {
//	 return (x * sin(x) + cos(x));
	return (exp(x) - (2 * x) - 1);
}

void regular_falsi(double a, double b) {
	FILE *fp = fopen("false_position_result.txt", "w");	// file to save the result in tabular form
	double c = 0,										// stores point where line from f(a) and f(b) intersects the x-axis
		   abs_error = 0,								// stores value of absolute error
   		   prev_error = 0,
		   convergence = 0,
		   prev_c = 0;									// saves previous value of c for calculating the absolute error
	char sign = '-';									// store sign of f(a)*f(b)
	int iter = 0;										// iteration

	
	fprintf(fp, "Given inputs a, b: %lf, %lf\n", a, b);
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	fprintf(fp, "|  iter  |          a  |           b  |           c  |        f(a)  |        f(c)  |   f(a)*f(c)  |     error  |   convergence   |\n");
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	
	c = (a * f(b) - b * f(a)) / (f(b) - f(a));
	prev_c = c;

//	while(f(c) > 0.0000005 || f(c) < -0.0000005) {
	do {
	++iter;
	sign = (f(a) * f(c) < 0) ? '-': '+';

		if(f(a) * f(c) < 0)
			b = c;									// change interval limit-2
		else
			a = c;									// change interval limit-1
		prev_c = c;
		c = (a * f(b) - b * f(a)) / (f(b) - f(a));
		prev_error = abs_error;
		abs_error = (c - prev_c);				// calculate absolute error
		convergence = fabs(log(fabs(abs_error))/log(fabs(prev_error)));
		fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.9lf  | %10.9lf |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), abs_error, convergence);
	} while(fabs(c - prev_c) > 0.0000005);

	fprintf(fp, "------------------------------------------------------------------------------------------------------------------------------------\n");	
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.9lf  |  %10.9lf  |\n", iter, a, b, c, f(a), f(c), (f(a)*f(c)), abs_error, convergence);
	fprintf(fp, "------------------------------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", c);
	printf("absolute error is: %lf\n", abs_error);

	fprintf(fp, "Root in the given interval: %lf\n", c);
	fprintf(fp, "absolute error is: %lf\n", abs_error);
	fclose(fp);
}

void bisection(double a, double b) {
	FILE *fp = fopen("bisection_result.txt", "w");	// file to save the result in tabular form
	double mid = 0,									// mid-value of a & b
		   abs_error = 0,							// stores value of absolute error
		   prev_error = 0,
		   convergence = 0,
		   prev_mid = 0;							// saves previous value of mid for calculating the absolute error
	char sign = '-';								// store sign of f(a)*f(b)
	int iter = 0;									// iteration
	
	fprintf(fp, "Given inputs a, b: %lf, %lf\n", a, b);
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	fprintf(fp, "|  iter  |          a  |           b  |         mid  |        f(a)  |      f(mid)  | f(a)*f(mid)  |    error      |  covergence  |\n");
	fprintf(fp, "---------------------------------------------------------------------------------------------------------------------------------\n");
	
	mid = (a + b) / 2;
	prev_mid = mid;

	do {
	++iter;
	sign = (f(a) * f(mid) < 0) ? '-': '+';

		if(f(a) * f(mid) < 0)
			b = mid;								// change interval limit-2
		else
			a = mid;								// change interval limit-1
		prev_mid = mid;
		mid = (a + b) / 2;
		prev_error = abs_error;
		abs_error = (mid - prev_mid);				// calculate absolute error
		convergence = fabs(log(fabs(abs_error)) / log(fabs(prev_error)));
		fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.9lf  |    %10.9lf    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), abs_error, convergence);
	} while(fabs(a - b) > 0.00000005);

	fprintf(fp, "--------------------------------------------------------------------------------------------------------------------------------\n");	
	fprintf(fp, "|  %4d  | %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.6lf  |  %10.9lf  |    %10.9lf    |\n", iter, a, b, mid, f(a), f(mid), (f(a)*f(mid)), abs_error, convergence);
	fprintf(fp, "--------------------------------------------------------------------------------------------------------------------------------\n");
	
	printf("Root in the given range is: %lf\n", mid);
	printf("absolute error is: %lf\n", abs_error);

	fprintf(fp, "Root in the given interval: %lf\n", mid);
	fprintf(fp, "absolute error is: %lf\n", abs_error);
	fclose(fp);
}

int main() {
	double a = 0.0, b = 0.0;
	do {
		printf("Enter a & b so that f(a)*f(b) is negative:\n");
		scanf("%lf""%lf", &a, &b);
	}
	while((f(a) * f(b)) > 0);
	printf("a = %lf\t\tb = %lf\n\t\tf(a)*f(b) = %lf\n", a, b, f(a)*f(b));
	printf("Reverse Falsi method:\n");
	regular_falsi(a, b);
	printf("Bisection mehod:\n");
	bisection(a, b);
	return 0;
}

// e^x - 2 *x -1
// 1 - 4
