#include <stdio.h>
#include <math.h>

#define g_x sqrt(exp(x) / 4)
#define dg_x (exp(x/2) / 4)
#define f1 (exp(x) - 4 * pow(x, 2))

double next_x(double x) {
	return g_x;
}

double f(double x) {
	return f1;
}

double dg(double x) {
	return dg_x;
}

double order_conv(double error, double prev_error) {
	return (log(error) / log(prev_error));
}

void fixed_point(double x) {
	double x2, error=0, prev_error=x, root = 0, conv;
	int root_found = 0;
	int iter = 0;
	FILE *fp = fopen("fixed_point_root.csv", "w");

	printf("Checking for positive x:\n");
	printf(" i|        x|    dg(x)|     f(x)|    abs_err| ord_conv\n");
	printf("------------------------------------------------------\n");
	do {
		++iter;
		x2 = next_x(x);
		error = fabs(x2 - x);
		if(f(x2) < 0.0000005 && f(x2) > -0.0000005) {
			root = x2;
			++root_found;
		}
		if(fabs(x2 - x) < 0.0000005) {
			root = x2;
			++root_found;
		}
		if(iter > 1) {
			conv = fabs(order_conv(error, prev_error));
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf| %12.10lf\n", iter, x2, dg(x2), f(x2), error, conv);
		}
		else
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf|     -    \n", iter, x2, dg(x2), f(x2), error, conv);
		prev_error = error;
		x = x2;
		fprintf(fp, "%d,%lf\n", iter, x);
	} while(iter <= 50 && !root_found);
	if (root_found)
		printf("Root: %lf\n", root);
	else
		printf("No root found.\n");

	fprintf(fp, "\n\n");
	printf("Checking for negative x:\n");
	iter = 0;
	root_found = 0;
	printf(" i|        x|    dg(x)|     f(x)|    abs_err| ord_conv\n");
	printf("------------------------------------------------------\n");
	do {
		++iter;
		x2 = -1 * next_x(x);
		error = fabs(x2 - x);
		if(f(x2) < 0.0000005 && f(x2) > -0.0000005) {
			root = x2;
			++root_found;
		}
		if(fabs(x2 - x) < 0.0000005) {
			root = x2;
			++root_found;
		}
		if(iter > 1) {
			conv = fabs(order_conv(error, prev_error));
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf| %10.8lf\n", iter, x2, dg(x2), f(x2), error, conv);
		}
		else
			printf("%2d| %8.6lf| %8.6lf| %8.6lf| %8.6lf|     -    \n", iter, x2, dg(x2), f(x2), error, conv);
		prev_error = error;
		x = x2;
		fprintf(fp, "%d,%lf,%lf\n", iter, x, error);
	} while(iter <= 50 && !root_found);
	if (root_found)
		printf("Root: %lf\n", root);
	else
		printf("No root found.\n");
}

int main() {
	double x = 0.0;
	printf("Enter your initial guess: ");
	scanf("%lf", &x);
	fixed_point(x);
	return 0;
}