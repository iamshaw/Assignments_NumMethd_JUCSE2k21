#include <stdio.h>
#include <math.h>

#define f1 (pow(x, 2) - 2)
#define df1 (2 * x)
#define ddf1 (2)

#define f2 (pow(x, 3) - 2 * x - 5)
#define df2 (3 * pow(x, 2) - 2)
#define ddf2 (6 * x)

#define f3 (x - 2 + log(x))
#define df3 (1 + (1 / x))
#define ddf3 (-1 / pow(x, 2))

#define f4 (pow(x, 3) - 3 * x - 5)
#define df4 (3 * pow(x, 2) - 3)
#define ddf4 (6 * x)

#define f5 ((x * sin(x)) + cos(x))
#define df5 (x * cos(x))
#define ddf5 (cos(x) - (x * sin(x)))

#define f6 (exp(x) - 2 * x - 1)
#define df6 (exp(x) - 2)
#define ddf6 (exp(x))

#define f7 (exp(x) - 4 * pow(x, 2))
#define df7 (exp(x) - 8 * x)
#define ddf7 (exp(x) - 8)

double f(double x) {
	return f7;
}

double df(double x) {
	return df7;
}

double ddf(double x) {
	return ddf7;
}

int main() {
	FILE *data = fopen("newton_root.csv", "w");
	double x1, x2, x_temp, error = 1, error_prev, ord_conv;
	int iter = 0;
	scanf("%lf", &x1);
	printf("   i         x_i      f(x_i)   abs_error    ord_conv\n");
	printf("----------------------------------------------------\n");
	do {
		++iter;
		error_prev = error;
		x_temp = x1;
		x2 = x1 - (f(x1) / df(x1));
		x1 = x2;
		error = x2 - x_temp;
		ord_conv = (log(fabs(error)) / log(fabs(error_prev)));
		if(iter == 1)
			printf("%4d  %10.6lf  %10.6lf  %10.6lf  ----na----\n", iter, x1, f(x1), error, ord_conv);
		else
			printf("%4d  %10.6lf  %10.6lf  %10.6lf  %10.6lf\n", iter, x1, f(x1), error, ord_conv);
		fprintf(data, "%d,%lf,%lf\n", iter, x2, error);
	} while(fabs(x2 - x_temp) > 0.00001);
	printf("----------------------------------------------------\n");
	printf("\nRoot is: %lf\n", x2);
	return 0;
}