#include <stdio.h>

#define dimen 10

int A[dimen][dimen], L[dimen][dimen], U[dimen][dimen], temp[dimen][dimen];

void display_mat(void *arr, int n, int size) {
    int (*mat)[size] = arr;
    int i, j;
    printf("\n\n");
    for(i = 0; i < n; ++i) {
        for(j = 0; j < n; ++j)
            printf("  %7d", mat[i][j]);
        printf("\n");
    }
    printf("\n\n");
}

void init_L(int n) {
    int i, j;
    for(i = 0; i < n; ++i)
        for(j = 0; j < n; ++j)
            if(i == j)
                L[i][j] = 1;
            else
                L[i][j] = 0;
}

void decompose(int n) {
    int i, j, k;
    double multiplier = 0;
    for(i = 0; i < n - 1; ++i) {
        for(j = i + 1; j < n; ++j) {
            if(U[i][i] == 0)
                multiplier = 0;
            else
                multiplier = U[j][i] / U[i][i];
            // printf("mult: %lf\n", multiplier);
            L[j][i] = multiplier;
            for(k = i; k < n; ++k) {
                U[j][k] = U[j][k] - (multiplier * U[i][k]);
                // printf("\nU:");
                // display_mat(U, n, dimen);
            }
        }
    }
}

int main() {
    int n;
    printf("Enter the dimension of square matrix: ");
    scanf("%d", &n);
    int i, j;
    for(i = 0; i < n; ++i)
        for(j = 0; j < n; ++j) {
            printf("Enter the element A[%d][%d]: ", i, j);
            scanf("%d", &A[i][j]);
            U[i][j] = A[i][j];
        }
    printf("\nA:");
    display_mat(A, n, dimen);
    printf("\nL:");
    init_L(n);
    display_mat(L, n, dimen);
    printf("\nU:");
    display_mat(U, n, dimen);
    printf("Decomposing A into L and U...\n");
    decompose(n);
    printf("\nL:");
    display_mat(L, n, dimen);
    printf("\nU:");
    display_mat(U, n, dimen);
    return 0;

}